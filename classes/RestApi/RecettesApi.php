<?php
namespace WerkRecipes\RestApi;

use WerkRecipes\Posttypes\Recipes;
use WP_REST_Request;

class RecettesApi
{
    private $namespace;
    private $route;
    public function __construct($namespace){
        $this->namespace = $namespace;
        $this->routes = '/recipes/';
        $this->register_recipes_routes();
    }    

    public function register_recipes_routes(){
        register_rest_route($this->namespace, $this->routes,[
            'methods' =>'GET',
            'callback' => [$this, 'get_all_recipes']
        ]);
        register_rest_route( $this->namespace, '/recipes/(?P<id>\d+)', [
            'methods' => 'GET', 
            'callback' => [$this, 'get_recipe_by_id']
        ]);
    }

    public function get_all_recipes(){
        $recipes = Recipes::get_all_recipes();
        $post_data = [];

        foreach($recipes->posts as $posts){
            $post_data[] = $this->werk_response_formatter($posts);
        }

        return rest_ensure_response($post_data);
    }
    public function get_recipe_by_id(WP_REST_Request $request){
        $id = $request['id'];
        $recipes = Recipes::get_recipe_by_id($id);
        $post_data = $this->werk_response_formatter($recipes->posts[0]);
        return rest_ensure_response($post_data);
    }

    public function werk_response_formatter($posts){
        $post_data = [];
        $post_id = $posts->ID;
        $post_author = $posts->post_author;
        $post_title = $posts->title; 
        $post_content = $posts->content;
        $post_permalink = get_the_permalink($posts);
        $post_thumbnail = get_the_post_thumbnail_url($posts);

        $post_data[$post_id]['author'] = $post_author;
        $post_data[$post_id]['title']   = $post_title;
        $post_data[$post_id]['content'] = $post_content;
        $post_data[$post_id]['permalink'] = $post_permalink;
        $post_data[$post_id]['thumbnail'] = $post_thumbnail;
        return $post_data;
    }

}
