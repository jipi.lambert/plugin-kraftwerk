<?php
namespace WerkRecipes\Blocks;

class BlocksInit
{
    public function __construct(){
        add_action('enqueue_block_editor_assets', [$this, 'werk_block_editor_assets']);
    }
    
    public function werk_block_editor_assets(){
     
        wp_register_script(
            'werk_block_bundle',
            plugins_url("/dist/bundle.js",__FILE__),
            ['wp-i18n', 'wp-element', 'wp-blocks', 'wp-components','wp-editor','wp-api'], 
            filemtime(plugin_dir_path('__DIR__').'classes/Blocks/dist/bundle.js')
        );
        wp_enqueue_script('werk_block_bundle');
    }
}
