//console.log(wp.blocks);
const { registerBlockType } = wp.blocks;
const { __ } = wp.i18n;
const { InspectorControls } = wp.editor;
const { PanelBody, PanelRow, SelectControl, TextControl } = wp.components;


registerBlockType(
    'werk/recipe',
    {
        title:  __('Recipe', 'werkrecipes'),
        description: __('Short recipe format','werkrecipes'),
        category:'common',
        icon: 'welcome-learn-more',
        keywords:[
            __('food','werkrecipes'),
            __('ingredients','werkrecipes'),
            __('mealtype','werkrecipes'),
        ],
        supports:{
            html:false
        },
        //Conteient les proprietes « props » (qui signifie « propriétés »)
        // Section visible dans l'editeur.
        attributes: {
            ingredients:{
                type:'string',
                source:'text',
                selector:'.ingredients-ph'
            }, 
            cooking_time:{
                type:'string',
                source:'text',
                selector:'.cooking-time-ph'
            },
            utensils:{
                type:'string',
                source:'text',
                selector:'.utensils-ph'
            },
            cooking_experience:{
                type:'string',
                source:'text',
                default:'beginner',
                selector:'.cooking-experience-ph'
            },
            meal_type:{
                type:'string',
                source:'text',
                default:'breakfast',
                selector:'.meal-type-ph'
            }
        },
        edit:( props ) => {
            // console.log(props);
            // Idee pour structurer le code et rendre le jsx moins lourd. question de lisibilite

            return [
                // Mettra des element dans le sidebar
                <InspectorControls>
                    <PanelBody title= {__('Basics','werkrecipes')}>
                        <PanelRow>
                            <p>{ __('Configurer le contenu de mon bloc ici','werkrecipes')}</p>
                        </PanelRow>
                        <TextControl
                            label={ __('Ingredients','werkrecipes')}
                            help={ __('Ex.: Tomate, huile, laitue, huile d\'olive','werkrecipes')}
                            value={props.attributes.ingredients}
                            onChange={   ( new_val ) => {
                                props.setAttributes({ingredients:new_val}) 
                            }}
                        />
                        <TextControl
                            label={ __('Cooking time','werkrecipes')}
                            help={ __('Temps de cuisson','werkrecipes')}
                            value= {props.attributes.cooking_time }
                            onChange={ ( new_val ) => {
                                props.setAttributes({cooking_time:new_val})
                            }}
                        />
                        <TextControl
                            label={ __('Utensils','werkrecipes')}
                            help={ __('Ex.: fourchette, couteau, poele, chaudron','werkrecipes')}
                            value={props.attributes.utensils}
                            onChange={ ( new_val ) => {
                                props.setAttributes({utensils:new_val})
                            }}
                        />
                        <SelectControl 
                            label={ __('Cooking experience','werkrecipes')}
                            help={ __('Quel niveau d\'expérience','werkrecipes')}
                            value={props.attributes.cooking_experience}
                            options={[
                                {value:'beginner', label:__('Beginner','werkrecipes')},
                                {value:'intermediate', label:__('Intermediate','werkrecipes')},
                                {value:'expert', label:__('Expert','werkrecipes')}
                            ]}
                            onChange={ ( new_val ) => {
                                props.setAttributes({cooking_experience:new_val})
                            }}
                        />
                        <SelectControl 
                            label={ __('Type de repas','werkrecipes')}
                            help={ __('Quel est le type de repas','werkrecipes')}
                            value={props.attributes.meal_type}
                            options={[
                                {value:'breakfast', label:__('Breakfast','werkrecipes')},
                                {value:'lunch', label:__('Lunch','werkrecipes')},
                                {value:'dinner', label:__('Dinner','werkrecipes')}
                            ]}
                            onChange={ ( new_val ) => {
                                props.setAttributes({meal_type:new_val})
                            }}
                        />
                    </PanelBody>
                </InspectorControls>,
                <div className={props.className}>
                    <ul className='list-unstyled'>
                        <li><strong>{__('Ingredients','werkrecipes')} :</strong>
                            <span className='ingredients-ph' >{props.attributes.ingredients}</span>
                        </li>
                        <li><strong>{__('Cooking time','werkrecipes') }: </strong>
                            <span className='cooking-time-ph' >{props.attributes.cooking_time }</span>
                        </li>
                        <li><strong>{__('Utensils','werkrecipes') }: </strong>
                            <span className='utensils-ph' >{props.attributes.utensils}</span>
                        </li>
                        <li><strong>{__('Cooking experience','werkrecipes') }:</strong>
                            <span className='cooking-experience-ph' >{props.attributes.cooking_experience}</span>
                        </li>
                        <li><strong>{__('Meal type','werkrecipes') }:</strong>
                            <span className='meal-type-ph' >{props.attributes.meal_type}</span>
                        </li>
                    </ul>
                </div>
            ];
        },
        save:( props ) => {
            return( 
                <div>
                    <ul className='list-unstyled'>
                        <li><strong>{__('Ingredients','werkrecipes')} :</strong>
                            <span className='ingredients-ph' >{props.attributes.ingredients}</span>
                        </li>
                        <li><strong>{__('Cooking time','werkrecipes') }: </strong>
                            <span className='cooking-time-ph' >{props.attributes.cooking_time }</span>
                        </li>
                        <li><strong>{__('Utensils','werkrecipes') }: </strong>
                            <span className='utensils-ph' >{props.attributes.utensils}</span>
                        </li>
                        <li><strong>{__('Cooking experience','werkrecipes') }:</strong>
                            <span className='cooking-experience-ph' >{props.attributes.cooking_experience}</span>
                        </li>
                        <li><strong>{__('Meal type','werkrecipes') }:</strong>
                            <span className='meal-type-ph' >{props.attributes.meal_type}</span>
                        </li>
                    </ul>
                </div>
            )
        }, 
       
    }
);