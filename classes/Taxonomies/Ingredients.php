<?php

namespace WerkRecipes\Taxonomies;

class Ingredients
{
    public function __construct(){
        $this->create_taxonomy();    
    }

    public function create_taxonomy()
    {
        $args = [
            'hierarchical'      => true,
            'label'             => __('Ingredients','werkrecipes'),
            'show_ui'           => true,
            'show_admin_column' => true,
            'show_in_menu'      => false,
            'show_in_nav_menus' => true,
            'show_in_rest'      => true,
            'query_var'         => true,
            'rewrite'           => ['slug' => 'type/ingredients']
        ];
        register_taxonomy('werk_ingredients', ['werk_recipes'], $args);
    }
}