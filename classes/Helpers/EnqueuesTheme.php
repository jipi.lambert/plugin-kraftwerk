<?php
namespace WerkRecipes\Helpers;

class EnqueuesTheme
{
    public function __construct(){
        $this->scripts();
    }    

    public function scripts(){
        wp_enqueue_script('script_main',plugins_url('WerkRecipes').'/scripts/ajax.js', ['jquery'], '0.1', true);
    }
}
