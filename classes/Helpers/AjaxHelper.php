<?php
namespace WerkRecipes\Helpers;

use WerkRecipes\Posttypes\Recipes;

class AjaxHelper
{
    public function add_new_recipe(){
        $response = [];
        check_ajax_referer('secure-nonce-name', 'security');

        if(empty($_POST["name"])){
            $response["error_message"][]="Votre nom est requis";
        }
        
        if(empty($_POST["email"])){
            $response["error_message"][]="Votre courriel est requis";
        }

        if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
            $response['error_message'][] = 'Courriel invalide';
        }
        
        if(empty($_POST["recipe_name"])){
            $response["error_message"][]="LE nom de recette est requis";
        }

        if(empty($_POST["recipe"])){
            $response["error_message"][]="Ca prend une recette...";
        }

        if (is_null($response['error_message'])){
            $recipe_id = Recipes::add_recipe($_POST["name"],$_POST["email"],$_POST["recipe_name"],$_POST["recipe"] );
        }
        if ( $recipe_id ){
            $response['success'] = "Votre recette a ete ajoute avec succès";
            exit(json_encode($response));
            wp_die();
        } else {
            exit(json_encode($response));
            wp_die();
        }

    }
}
