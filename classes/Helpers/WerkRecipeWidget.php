<?php
namespace WerkRecipes\Helpers;

use WerkRecipes\Posttypes\Recipes;
use WP_Widget;

class WerkRecipeWidget extends WP_Widget{
    public function __construct(){
        parent::__construct(
            'werk_recipe_widget',
            'Werk_Recipe_Widget',
            ['description' => __('Latest Recipes', 'werkrecipes')]

        );   
    }    
    /**formulaire en backend */
    public function form($instance){
        $title = !empty($instance['title']) ? $instance['title'] : esc_html__('','werkrecipes');
        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php echo esc_html__( 'Title:', 'werkrecipes' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>Ce widget afficher les nouvelles recettes</p>
        <?php
    }
    /**Sauvegarde des donnes */
    public function update($new_instance, $old_instance){
        $instance = [];
        $instance['title'] =  ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';  
        return $instance;
    }

    /** front */
    public function widget($args, $instance){
        $data = Recipes::get_latest_recipes();
        echo "<hr>";
        echo $args['before_widget'];
        if ( !empty($instance['title'])){
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        if($data->have_posts()){
            while($data->have_posts()){
                $data->the_post();
                ?>
                    <p class="blog-post_title"><a href="<?php echo the_permalink();?>"><?php the_title();?></a></p>
                <?php
            }
        }
        
        echo $args['after_widget'];
        echo "<hr>";
    }
}
