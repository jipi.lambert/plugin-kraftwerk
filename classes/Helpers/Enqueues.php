<?php
namespace WerkRecipes\Helpers;

class Enqueues
{
    public function __construct(){
        $this->scripts();
    }    

    public function scripts(){
        wp_enqueue_script('sub_menu_hack',plugins_url('WerkRecipes').'/scripts/menu-helper.js', ['jquery'], '0.1', true);
    }
}
