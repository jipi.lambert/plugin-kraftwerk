<?php

namespace WerkRecipes\Menu;

class AdminMenu
{
    public function __construct()
    {
        $this->add_menu_pages();
    }

    public function add_menu_pages()
    {
        add_menu_page(
            'Werk Recipes',
            'Werk Recipes',
            'manage_options',
            'werk_recipes_menu',
            [$this,'custom_menu_page'],
            'dashicons-category',
            30
        );

        add_submenu_page(
            'werk_recipes_menu',
            'Recipes',
            'Recipes',
            'edit_posts',
            'edit.php?post_type=werk_recipes',
            false
        );

        add_submenu_page(
            'werk_recipes_menu',
            'Modules',
            'Modules',
            'edit_posts',
            'edit.php?post_type=werk_modules',
            false
        );

        add_submenu_page(
            'werk_recipes_menu',
            'Ingredients',
            'Ingredients',
            'edit_posts',
            'edit-tags.php?taxonomy=werk_ingredients&post_type=werk_recipes',
            false
        );
    }

    public function custom_menu_page()
    {?>
        <div>
            <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
            <form action="options.php" method="post">
            <?php
                settings_fields('werk_recipes_fields');
                do_settings_sections('werk_recipes_fields');
                submit_button();
            ?>
            </form>
        </div>
    <?php
    }
}
