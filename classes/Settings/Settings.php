<?php
namespace WerkRecipes\Settings;

class Settings
{
    public function __construct(){
        $this->werk_settings_setup();
    }
    public function werk_settings_setup(){
        add_settings_section(
            'werk_settings_section',
            'Les options',
            [$this, 'werk_settings_section'],
            'werk_recipes_fields'
        );

        add_settings_field(
            'werk_delete_field',
            'Detruire les donnees du plugin lors de la desinstallation',
            [$this, 'werk_recipe_setting_form'],
            'werk_recipes_fields',
            'werk_settings_section'
        );
        register_setting('werk_recipes_fields',  'werk_delete_field');
    }

    public function werk_settings_section(){
        echo '<h2>Section destruction</h2>';
    }

    public function werk_recipe_setting_form(){
        $option = get_option('werk_delete_field');
        $checked = $option == "on" ? 'checked="checked"' : '';

        echo "<input type='checkbox' id='werk_delete_field' name='werk_delete_field' $checked >";
    }
}
