<?php

namespace WerkRecipes\Roles;

class Roles{
    public function __construct(){
        $this->student_role();
        $this->teacher_role();
    }

    public function student_role(){
        add_role(
            'werk-student',
            __('Werk Student', 'werkrecipes'),
            [
                'read' => true,
                'edit_posts' => false,
                'delete_posts' => false
            ]
        );
    }

    public function teacher_role(){
        add_role(
            'werk-teacher',
            __('Werk Teacher', 'werkrecipes'),
            [
                'read' => true,
                'edit_posts' => true,
                'delete_posts' => true
            ]
        );
    }
}