<?php

namespace WerkRecipes\Data;

class Ratings
{
    public static function addRating($recipe_id, $rating)
    {
        global $wpdb;
   
        $table = $wpdb->prefix . "werk_ratings";
        $rating_int = intval($rating);

        $insert = $wpdb->insert(
            $table,
            [
                'recipe_id' => $recipe_id,
                'rating'    => $rating
            ],[
                '%d',
                '%d'
            ]
        );
    }
}