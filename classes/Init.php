<?php
namespace WerkRecipes;


use WerkRecipes\Helpers\AjaxHelper;
use WerkRecipes\Helpers\Enqueues;
use WerkRecipes\Helpers\EnqueuesTheme;
use WerkRecipes\Helpers\Templater;
use WerkRecipes\Menu\AdminMenu;
use WerkRecipes\Metaboxes\RecipesMetabox;
use WerkRecipes\Posttypes\Modules;
use WerkRecipes\Posttypes\Recipes;
use WerkRecipes\RestApi\RecettesApi;
use WerkRecipes\Roles\Roles;
use WerkRecipes\Settings\Settings;
use WerkRecipes\Taxonomies\Ingredients;
use WerkRecipes\Blocks\BlocksInit;
class Init
{
    private $templater;
    private $ajax;

    public function __construct(){
        $this->templater = new Templater();
        $this->ajax = new AjaxHelper();
        new BlocksInit();
        add_action('init', [$this,'create_posttypes']);
        add_action('init', [$this,'create_taxonomies']);
        add_action('init', [$this, 'manage_roles']);
        add_action('add_meta_boxes', [$this,'add_metaboxes']);
        add_action('admin_menu', [$this, 'create_admin_menu']);
        add_action('admin_enqueue_scripts', [$this, 'helpers_enqueue_scripts']);
        add_action('wp_enqueue_scripts', [$this, 'theme_enqueue_scripts']);
        add_action('save_post_werk_recipes', [$this, 'save_meta']);
        add_action('admin_init', [$this, 'settings_init']);
        add_action('rest_api_init', [$this, 'werk_recipes_routes']);
        add_shortcode('werk_search_form', [$this, 'werk_search_shortcode']);

        add_action('widgets_init', [$this, 'register_werk_widget']);
        
        add_action('wp_head',[$this, 'ajax_variables']);
        add_shortcode('werk_recipes_form', [$this, 'werk_recipes_shortcode']);
        add_action("wp_ajax_werk_submit", [$this, 'ajax_new_recipe']);
        add_action("wp_ajax_nopriv_werk_submit", [$this, 'ajax_new_recipe']);

    }

    public function create_posttypes(){
        new Recipes();
        new Modules();
    }
    
    public function create_taxonomies(){
        new Ingredients();
    }

    public function create_admin_menu(){
        new AdminMenu();
    }

    public function helpers_enqueue_scripts(){
        new Enqueues();
    }

    public function theme_enqueue_scripts(){
        new EnqueuesTheme();
    }
    
    public function manage_roles(){
        new Roles();
    }

    public function add_metaboxes(){
        $this->metaboxes = new RecipesMetabox();
    }

    public function save_meta($post_id){
        RecipesMetabox::werk_rating_save($post_id, $_POST);
    }

    public function settings_init(){
        new Settings();
    }
    public function werk_recipes_routes(){
       new RecettesApi("werk/v1");
    }

    public function ajax_variables(){
        ?>
        <script type="text/javascript">
            var ajax_url = '<?php echo admin_url("admin-ajax.php") ?>';
            var ajax_nonce = '<?php echo wp_create_nonce("secure-nonce-name")?>';
        </script>
        <?php
    }

    public function ajax_new_recipe(){
        $this->ajax->add_new_recipe();
    }

    public function werk_search_shortcode(){
        return $this->templater->werk_get_template("search-form.php");
    } 
    public function werk_recipes_shortcode(){
        return $this->templater->werk_get_template("recipe-form.php");
    } 

    public function register_werk_widget(){
            register_widget('WerkRecipes\Helpers\WerkRecipeWidget');   
    }
}
