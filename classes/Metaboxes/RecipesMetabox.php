<?php
namespace WerkRecipes\Metaboxes;
use WerkRecipes\Data\Ratings;

class RecipesMetabox
{
    public function __construct(){
        $this->add_rating_metabox();
    }    

    public function add_rating_metabox(){
        add_meta_box(
            'werk_recipe_ratings',
            'Ratings',
            [$this,'werk_rating_html'],
            'werk_recipes',
            'normal',
            'low'
        );
    }

    public function werk_rating_html(){
        global $post;
     

        echo "<select name='rating' id='rating'>
            <option value='1' class='1-option'>1</option>
            <option value='2' class='2-option'>2</option>
            <option value='3' class='3-option'>3</option>
            <option value='4' class='4-option'>4</option>
            <option value='5' class='5-option'>5</option>
        </select>";
    }

    public static function werk_rating_save($post_id){
        $rating = $_POST['rating'];
        Ratings::addRating($post_id, $rating);
        // add_post_meta($post_id, 'rating', $rating);
            
    }
}
