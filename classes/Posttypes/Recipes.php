<?php
namespace WerkRecipes\Posttypes;
use WP_Query;
class Recipes
{
    public function __construct(){
        $this->create_post_type();
        add_action('add_meta_boxes', [$this,'add_metabox']);
    }

    public function create_post_type(){
        $labels = [
            'name'               => __("Recipes", "werkrecipes"),
            'singular_name'      => __("Recipes", "werkrecipes"),
            'menu_name'          =>  _x("Recipes", "werkrecipes"),
            'add_new'            => __("Ajouter un nouveau Recipes", "werkrecipes"),
            'add_new_item'       => __("Ajouter un nouveau Recipes", "werkrecipes"),
            'edit'               => __("Editer", "werkrecipes"),
            'edit_item'          => __("Editer", "werkrecipes"),
            'new_item'           => __("Nouveau", "werkrecipes"),
            'view'               => __("Voir", "werkrecipes"),
            'view_item'          => __("Voir", "werkrecipes"),
            'search_items'       => __("Recherche dans les Recipes", "werkrecipes"),
            'not_found'          => __("Non trouve", "werkrecipes"),
            'not_found_in_trash' => __("Aucun element trouve dans la poubelle", "werkrecipes"),
            'parent'             => __("Recipes parent", "werkrecipes")
        ];
        

        $args = [
            'labels' => $labels,
            'public' => true,
            'has_archive'   => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_in_menu' => false,
            'hierarchical'  => false,
            'show_in_rest' => false,
            'supports'  => ['editor'],
            'rewrite'   => ["slug" => "recipes"],
            'capability' => 'post'
        ];
        register_post_type('werk_recipes', $args);
    }

    public function add_metabox(){
        add_meta_box(
            'werk_recipe_author',
            __('Recipe Author',"werkrecipes" ),
            [$this, 'werk_recipe_author'],
            'werk_recipes',
            'normal',
            'default'
        );
    }

    public function werk_recipe_author(){
       $post_meta = get_post_meta(get_the_ID(),'werk_recipe_author');

       echo '<input type="text" name="author" value"'. esc_textarea($post_meta[0]).'" class="widefat">';
    }

    public static function get_latest_recipes(){
        $args = [
            'post_type' => 'werk_recipes',
            'posts_per_page' => 5
        ];

        return new WP_Query($args);
    }

    public static function get_all_recipes(){
        $args = [
            'post_type' => 'werk_recipes',
            'posts_per_page' => -1
        ];

        return new WP_Query($args);
    }

    public static function get_recipe_by_id($id){
        $args = [
            'post_type' => 'werk_recipes',
            'p' => $id
        ];

        return new WP_Query($args);
    }

    public static function add_recipe( $name, $email, $recipe_name, $recipe){
        $args = [
            'comment_status' => 'closed',
            'ping_status'   => 'closed',
            'post_author'   => 1,
            'post_name'     => $recipe_name ." par ". $name,
            'post_title'     => $recipe_name ." par ". $name,
            'post_status'   =>'draft',
            'post_type'     => 'werk_recipes',
            'post_content'  => $recipe
        ];
        $post_id = wp_insert_post($args);
        add_post_meta($post_id, 'werk_recipe_author', $name ."--". $email);
        return $post_id;
    }
}
