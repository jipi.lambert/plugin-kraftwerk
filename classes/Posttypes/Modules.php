<?php
namespace WerkRecipes\Posttypes;

class Modules
{
    public function __construct(){
        $this->create_post_type();
    }
    
    public function create_post_type(){
        $labels = [
            'name' => __("Module", "werkrecipes"),
            'singular_name' => __("Modules", "werkrecipes"),
            'menu_name' =>  _x("Modules", "werkrecipes"),
            'add_new'            => __("Ajouter un nouveau module", "werkrecipes"),
            'add_new_item'       => __("Ajouter un nouveau module", "werkrecipes"),
            'edit'               => __("Editer", "werkrecipes"),
            'edit_item'          => __("Editer", "werkrecipes"),
            'new_item'           => __("Nouveau", "werkrecipes"),
            'view'               => __("Voir", "werkrecipes"),
            'view_item'          => __("Voir", "werkrecipes"),
            'search_items'       => __("Recherche dans les modules", "werkrecipes"),
            'not_found'          => __("Non trouve", "werkrecipes"),
            'not_found_in_trash' => __("Aucun element trouve dans la poubelle", "werkrecipes"),
            'parent'             => __("Module parent", "werkrecipes")
        ];

        $args = [
            'labels' => $labels,
            'public' => true,
            'has_archive'   => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_in_menu' => false,
            'hierarchical'  => false,
            'rewrite'   => ["slug" => "modules"],
            'capability' => 'post'
        ];
        register_post_type('werk_modules', $args);
    }
}
