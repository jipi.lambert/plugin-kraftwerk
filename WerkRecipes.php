<?php 
/* 
    Plugin Name: WerkRecipes
    Plugin URI: http://www.kraftwerk.org 
    Description: Créer un plugin de recettes
    Author: jplambert 
    Version: 1.0 
    Text Domain:werkrecipes
    Author URI: http://www.kraftwerk.com 
*/
require __DIR__ . '/vendor/autoload.php';
use WerkRecipes\Init;

register_deactivation_hook( __FILE__, 'remove_roles' );
register_activation_hook(__FILE__, 'create_werk_table');
register_deactivation_hook(__FILE__, 'delete_werk_table');

function remove_roles(){
    remove_role( 'werk-student' );
    remove_role( 'werk-teacher' );
}

function create_werk_table()
{
    global $wpdb;
    $create = "CREATE TABLE `" . $wpdb->prefix . "werk_ratings`(
        `ID` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
        `recipe_id` BIGINT(20) UNSIGNED NOT NULL,
        `rating` FLOAT(3,2) UNSIGNED NOT NULL,
        PRIMARY KEY (`ID`)
    ) ENGINE=InnoDB " . $wpdb->get_charset_collate() . ";";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($create);
}

function delete_werk_table()
{
    if(get_option('werk_delete_field') == 'on'){
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS " . $wpdb->prefix . "werk_ratings" );
        delete_option('werk_delete_field');
    }
}

new Init();